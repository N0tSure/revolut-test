package com.revolut.test.service;

import com.revolut.test.model.Account;
import com.revolut.test.model.BalanceItem;
import com.revolut.test.model.Locks;
import com.revolut.test.repository.AccountRepository;
import com.revolut.test.repository.BalanceItemRepository;
import com.revolut.test.repository.BalanceItemRepository.BalanceItemKey;
import com.revolut.test.repository.Page;

import javax.inject.Inject;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * <p>
 * Created on 17.09.2018.
 * </p>
 *
 * Provides API for operations with {@link Account} and it's dependent
 * component: {@link BalanceItem}.
 *
 * @author Artemis A. Sirosh
 */
public class AccountService {

    private final AccountRepository accountRepository;
    private final BalanceItemRepository balanceItemRepository;

    @Inject
    public AccountService(AccountRepository accountRepository, BalanceItemRepository balanceItemRepository) {
        this.accountRepository = accountRepository;
        this.balanceItemRepository = balanceItemRepository;
    }

    /**
     * Deletes {@link Account} and related {@link BalanceItem}.
     * @param id account identifier
     */
    public void deleteById(final UUID id) {

        final Lock lock = Locks.getAccountLock();
        try {
            lock.lock();
            accountRepository.deleteById(id);
            StreamSupport.stream(balanceItemRepository.findByAccountId(id).spliterator(), false)
                    .forEach(item -> balanceItemRepository.deleteById(BalanceItemRepository.toBalanceItemKey(item)));
        } finally {
            lock.unlock();
        }

    }

    /**
     * Return paginated accounts with related {@link BalanceItem}.
     * @param page pagination descriptor
     * @return paginated accounts
     */
    public Iterable<Account> findAll(final Page page) {

        final Lock lock = Locks.getAccountLock();
        try {
            lock.lock();
            return StreamSupport.stream(accountRepository.findAll(page).spliterator(), false)
                    .peek(account -> {
                        final Iterable<BalanceItem> items = balanceItemRepository.findByAccountId(account.getId());
                        account.setBalanceItems(
                                StreamSupport.stream(items.spliterator(), false)
                                        .collect(Collectors.toSet())
                        );
                    }).collect(Collectors.toList());

        } finally {
            lock.unlock();
        }

    }

    /**
     * Allows to find {@link Account} by it's identifier and related
     * {@link BalanceItem} items.
     *
     * @param id account identifier
     * @return account if exists
     */
    public Optional<Account> findById(final UUID id) {

        final Lock lock = Locks.getAccountLock();
        try {
            lock.lock();
            final Optional<Account> maybeAccount = accountRepository.findById(id);
            if (maybeAccount.isPresent()) {
                Account result = maybeAccount.get();
                result.setBalanceItems(
                        StreamSupport.stream(balanceItemRepository.findByAccountId(result.getId()).spliterator(), false)
                                .collect(Collectors.toSet()));

                return Optional.of(result);
            }

        } finally {
            lock.unlock();
        }

        return Optional.empty();
    }

    /**
     * Allows to persist account modal and merge all it's {@link BalanceItem}
     * items.
     * @param account new or updated {@link Account}
     * @return updated or save account
     */
    public Account save(final Account account) {

        final Lock lock = Locks.getAccountLock();
        try {
            lock.lock();
            final Account savedAccount = accountRepository.save(account);
            final Set<BalanceItemKey> newItemsKeys = account.getBalanceItems().stream()
                    .map(BalanceItemRepository::toBalanceItemKey)
                    .collect(Collectors.toSet());

            Iterable<BalanceItem> existedBalanceItems = balanceItemRepository.findByAccountId(savedAccount.getId());
            StreamSupport.stream(existedBalanceItems.spliterator(), false)
                    .filter(item -> !newItemsKeys.contains(BalanceItemRepository.toBalanceItemKey(item)))
                    .forEach(item -> balanceItemRepository.deleteById(BalanceItemRepository.toBalanceItemKey(item)));

            savedAccount.setBalanceItems(
                    account.getBalanceItems().stream()
                            .map(balanceItemRepository::save)
                            .collect(Collectors.toSet())
            );

            return savedAccount;

        } finally {
            lock.unlock();
        }

    }
}
