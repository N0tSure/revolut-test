package com.revolut.test.service;

import com.revolut.test.model.*;
import com.revolut.test.model.Currency;
import com.revolut.test.repository.CurrencyRepository;
import com.revolut.test.repository.Page;
import com.revolut.test.repository.TransferRepository;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>
 * Created on 18.09.2018.
 * </p>
 *
 * Given service provides API for performing transfers between accounts.
 *
 * @author Artemis A. Sirosh
 */
public class TransferService {

    private static final long TRANSFER_AWAITING_MICROSECONDS = 500L;

    private final AccountService accountService;
    private final CurrencyRepository currencyRepository;
    private final TransferRepository transferRepository;

    @Inject
    public TransferService(
            AccountService accountService,
            CurrencyRepository currencyRepository,
            TransferRepository transferRepository
    ) {
        this.accountService = accountService;
        this.currencyRepository = currencyRepository;
        this.transferRepository = transferRepository;
    }

    public @NotNull Optional<Transfer> findById(@NotNull UUID id) {

        final Lock lock = Locks.getTransferLock();
        try {
            lock.lock();
            return transferRepository.findById(id);
        } finally {
            lock.unlock();
        }

    }

    public @NotNull Iterable<Transfer> findAll(@NotNull Page page) {

        final Lock lock = Locks.getTransferLock();
        try {
            lock.lock();
            return transferRepository.findAll(page);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Performing transfer, takes {@link Transfer} and validate: sender's
     * account, receiver's account and transfer currency existence. If all
     * items before presented checks sender's balance for fund sufficiency.
     *
     * After all validation perform transfer.
     *
     * Await for acquiring of {@link Currency}, {@link Account} and
     * {@link Transfer} locks. If in
     * {@link TransferService#TRANSFER_AWAITING_MICROSECONDS} lock does not be
     * acquired transfer will fail.
     *
     * @param transfer new {@link Transfer}
     * @return result of transfer {@link TransferResult}
     */
    public @NotNull TransferResult perform(@NotNull Transfer transfer) {

        final ReentrantLock accountLock = Locks.getAccountLock();
        final ReentrantLock currencyLock = Locks.getCurrencyLock();
        final ReentrantLock transferLock = Locks.getTransferLock();
        final long stopTime = System.nanoTime() + TimeUnit.MILLISECONDS.toNanos(TRANSFER_AWAITING_MICROSECONDS);
        boolean lockAcquired;

        do {
            lockAcquired = accountLock.tryLock() && currencyLock.tryLock() && transferLock.tryLock();
            if (lockAcquired) {
                try {
                    return checkAndPerform(transfer);
                } finally {
                    accountLock.unlock();
                    currencyLock.unlock();
                    transferLock.unlock();
                }
            }

            if (accountLock.isHeldByCurrentThread())
                accountLock.unlock();

            if (currencyLock.isHeldByCurrentThread())
                accountLock.unlock();

            if (transferLock.isHeldByCurrentThread())
                transferLock.unlock();

            if (System.nanoTime() > stopTime) {
                throw new RuntimeException("Transfer failed: " + transfer);
            }
        } while (!lockAcquired);


        throw new IllegalStateException("Transfer failed: " + transfer);
    }

    /**
     * Check {@link Transfer} attributes and if they are satisfy criteria perform
     * transfer.
     *
     * @param transfer new transfer
     * @return result of transfer
     */
    @Contract("!null -> new")
    private @NotNull TransferResult checkAndPerform(@NotNull Transfer transfer) {

        final Optional<Account> maybeSenderAccount = accountService.findById(transfer.getSenderAccountId());
        if (!maybeSenderAccount.isPresent()) {
            return TransferResult.builder()
                    .withError("Account not existed: " + transfer.getSenderAccountId())
                    .build();
        }

        final Optional<Account> maybeReceiverAccount = accountService.findById(transfer.getReceiverAccountId());
        if (!maybeReceiverAccount.isPresent()) {
            return TransferResult.builder()
                    .withError("Account not existed: " + transfer.getSenderAccountId())
                    .build();
        }

        final Optional<Currency> maybeCurrency = currencyRepository.findById(transfer.getCurrencyId());
        if (!maybeCurrency.isPresent()) {
            return TransferResult.builder()
                    .withError("Currency not existed: " + transfer.getCurrencyId())
                    .build();
        }

        final Optional<BalanceItem> maybeSenderBalance = maybeSenderAccount.get().getBalanceItems()
                .stream()
                .filter(item -> maybeCurrency.get().getId().equals(item.getCurrencyId()))
                .filter(item -> item.getValue().compareTo(transfer.getFunds()) >= 0)
                .findFirst();

        final BalanceItem receiverBalanceItem = maybeReceiverAccount.get().getBalanceItems().stream()
                .filter(item -> item.getCurrencyId().equals(maybeCurrency.get().getId()))
                .findFirst()
                .orElse(BalanceItem.of(
                        maybeReceiverAccount.get().getId(), maybeCurrency.get().getId(), BigDecimal.ZERO
                ));

        return maybeSenderBalance.map(balanceItem -> perform(
                maybeSenderAccount.get(),
                maybeReceiverAccount.get(),
                balanceItem,
                receiverBalanceItem,
                transfer
        )).orElseGet(() -> TransferResult.builder()
                .withError("Insufficient funds to perform operation.")
                .build()
        );
    }

    /**
     * Perform transfer. All items implied valid.
     *
     * @param senderAccount account of transfer initiator
     * @param receiverAccount account of transfer receiver
     * @param senderBalance balance of sender for transfer currency
     * @param receiverBalance receiver's balance for transfer currency
     * @param transfer {@link Transfer} instance
     * @return result of transfer {@link TransferResult}
     */
    private @NotNull TransferResult perform(
            @NotNull final Account senderAccount,
            @NotNull final Account receiverAccount,
            @NotNull final BalanceItem senderBalance,
            @NotNull final BalanceItem receiverBalance,
            @NotNull final Transfer transfer) {

        final Set<BalanceItem> senderBalanceItemsCopy = new HashSet<>(senderAccount.getBalanceItems());
        final Set<BalanceItem> receiverBalanceItemsCopy = new HashSet<>(receiverAccount.getBalanceItems());
        senderBalanceItemsCopy.remove(senderBalance);
        receiverBalanceItemsCopy.remove(receiverBalance);

        senderBalance.setValue(senderBalance.getValue().subtract(transfer.getFunds()));
        receiverBalance.setValue(receiverBalance.getValue().add(transfer.getFunds()));

        if (senderBalance.getValue().compareTo(BigDecimal.ZERO) > 0)
            senderBalanceItemsCopy.add(senderBalance);

        receiverBalanceItemsCopy.add(receiverBalance);

        senderAccount.setBalanceItems(senderBalanceItemsCopy);
        receiverAccount.setBalanceItems(receiverBalanceItemsCopy);

        accountService.save(senderAccount);
        accountService.save(receiverAccount);

        transfer.setAppliedAt(Instant.now());
        Transfer savedTransfer = transferRepository.save(transfer);

        return TransferResult.builder()
                .withTransfer(savedTransfer)
                .build();
    }
}
