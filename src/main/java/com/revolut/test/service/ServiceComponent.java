package com.revolut.test.service;

import com.revolut.test.repository.CurrencyRepository;
import com.revolut.test.repository.InMemRepositoriesModule;
import com.revolut.test.repository.RepositoriesInitializer;
import dagger.Component;

import javax.inject.Singleton;

/**
 * <p>
 * Created on 20.09.2018.
 * </p>
 *
 * Provides service instances.
 *
 * @author Artemis A. Sirosh
 */
@Singleton
@Component(modules = {InMemRepositoriesModule.class})
public interface ServiceComponent {

    /**
     * Provides {@link AccountService} instance.
     * @return account service
     */
    AccountService accountService();

    /**
     * Provides {@link TransferService} instance.
     * @return transfer instance
     */
    TransferService transferService();

    /**
     * Provides {@link CurrencyRepository} instance.
     * @return currency repository
     */
    CurrencyRepository currencyRepository();

    /**
     * Creates test data initializer.
     * @return {@link RepositoriesInitializer} instance
     */
    RepositoriesInitializer repositoriesInitializer();
}
