package com.revolut.test.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created on 15 Sep, 2018.
 *
 * Represents account.
 *
 * @author Artemis A. Sirosh
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {

    /**
     * Accounts identifier.
     */
    @NonNull
    private UUID id;

    /**
     * Set of accounts balance items in particular currencies {@link Currency}.
     */
    @NonNull
    private Set<BalanceItem> balanceItems;

    /**
     * Creates new {@link Account} instance with random UUID as id and empty
     * balance item's set.
     *
     * @return new account instance
     */
    @Contract(" -> new")
    public static @NotNull Account newAccount() {
        return new Account(UUID.randomUUID(), new HashSet<>());
    }

    /**
     * Creates new instance of account, which already exists.
     *
     * @param id account's id
     * @param balanceItems account's balance
     * @return new instance of already existed account
     */
    @Contract("!null, !null -> new")
    public static @NotNull Account existsAccount(@NonNull UUID id, @NonNull Collection<BalanceItem> balanceItems) {
        return new Account(id, new HashSet<>(balanceItems));
    }

    /**
     * Add a new balance item {@link BalanceItem} to this account.
     * @param item balance item
     */
    public void addBalanceItem(@NonNull BalanceItem item) {
        this.balanceItems.add(item);
    }
}
