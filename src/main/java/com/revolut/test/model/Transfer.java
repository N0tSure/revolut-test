package com.revolut.test.model;

import lombok.*;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

/**
 * Created on 15 Sep, 2018.
 *
 * Represents transfer of particular currency {@link Currency} amount between
 * accounts {@link Account}.
 *
 * @author Artemis A. Sirosh
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transfer {

    /**
     * Transfer identifier.
     */
    @NonNull
    private UUID id;

    /**
     * Funds sender's account identifier.
     */
    @NonNull
    private UUID senderAccountId;

    /**
     * Funds receiver's account identifier.
     */
    @NonNull
    private UUID receiverAccountId;

    /**
     * Funds currency identifier.
     */
    @NonNull
    private String currencyId;

    /**
     * Amount of particular currency which will be transferred.
     */
    @NonNull
    private BigDecimal funds;

    /**
     * Timestamp representing date and time, when transfer has been closed.
     */
    private Instant appliedAt;

    /**
     * Creates new transfer from given parameters.
     *
     * @param senderAccountId sender account identifier
     * @param receiverAccountId receiver account identifier
     * @param currencyId currency identifier
     * @param funds amount of particular currency
     * @return new transfer instance
     */
    @Contract("!null, !null, !null, !null -> new")
    public static @NotNull Transfer newTransfer(
            @NonNull UUID senderAccountId,
            @NonNull UUID receiverAccountId,
            @NonNull String currencyId,
            @NonNull BigDecimal funds
    ) {
        return new Transfer(
                UUID.randomUUID(), senderAccountId, receiverAccountId, currencyId, funds, null
        );
    }

    /**
     * Allows instantiate existed {@link Transfer}.
     *
     * @param transferId transfer identifier
     * @param senderAccountId sender's account identifier
     * @param receiverAccountId receiver's account identifier
     * @param currencyId currency identifier
     * @param funds amount of funds in particular currency
     * @param appliedAt date/time where transfer applied
     * @return instance of transfer
     */
    @NotNull
    @Contract("!null, !null, !null, !null, !null, _ -> new")
    public static Transfer appliedTransfer(
            @NotNull UUID transferId,
            @NotNull UUID senderAccountId,
            @NotNull UUID receiverAccountId,
            @NotNull String currencyId,
            @NotNull BigDecimal funds,
            @Nullable Instant appliedAt
    ) {
        return new Transfer(transferId, senderAccountId, receiverAccountId, currencyId, funds, appliedAt);
    }

    /**
     * Set this transfer date/time when transfer has been closed.
     * @param appliedAt date/time
     */
    public void setAppliedAt(@Nullable Instant appliedAt) {
        this.appliedAt = appliedAt;
    }

    /**
     * Returns timestamp of transfer closing date/time. If transfer not closed
     * returns empty {@link Optional}.
     *
     * @return transfer closing date/time
     */
    public Optional<Instant> getAppliedAt() {
        return Optional.ofNullable(appliedAt);
    }
}
