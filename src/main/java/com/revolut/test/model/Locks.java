package com.revolut.test.model;

import org.jetbrains.annotations.Contract;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created on 24 Sep, 2018.
 *
 * Provides {@link java.util.concurrent.locks.Lock} for operations with
 * {@link Account}, {@link Currency} and {@link Transfer}.
 *
 * @author Artemis A. Sirosh
 */
public class Locks {

    private final static ReentrantLock ACCOUNT_LOCK = new ReentrantLock();
    private final static ReentrantLock CURRENCY_LOCK = new ReentrantLock();
    private final static ReentrantLock TRANSFER_LOCK = new ReentrantLock();

    /**
     * Returns {@link ReentrantLock} for operations with {@link Account}.
     * @return lock for operations with accounts
     */
    @Contract(pure = true)
    public static ReentrantLock getAccountLock() {
        return ACCOUNT_LOCK;
    }

    /**
     * Returns {@link ReentrantLock} for operations with {@link Currency}.
     * @return lock for operations with currencies
     */
    @Contract(pure = true)
    public static ReentrantLock getCurrencyLock() {
        return CURRENCY_LOCK;
    }

    /**
     * Returns {@link ReentrantLock} for operations with {@link Transfer}
     * @return lock for transfers
     */
    @Contract(pure = true)
    public static ReentrantLock getTransferLock() {
        return TRANSFER_LOCK;
    }


    @Contract(" -> fail")
    private Locks() {
        throw new UnsupportedOperationException("Wrong usage.");
    }
}
