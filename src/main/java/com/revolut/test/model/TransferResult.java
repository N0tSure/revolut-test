package com.revolut.test.model;

import com.google.common.collect.ImmutableList;
import lombok.Value;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * Created on 18.09.2018.
 * </p>
 *
 * Represent transfer result, if transfer rejected contain error descriptions
 * list. If no errors occurs contain {@link Transfer}.
 *
 * @author Artemis A. Sirosh
 */
@Value
public class TransferResult {

    /**
     * List of error descriptions.
     */
    private List<String> errors;

    /**
     * Transfer, if rejected, i.e. {@link #isRejected()} returns {@code true},
     * will be {@code null}.
     */
    private Transfer transfer;

    /**
     * Creates builder for {@link TransferResult}.
     * @return a new builder instance
     */
    @Contract(" -> new")
    public static @NotNull Builder builder() {
        return new Builder();
    }

    private TransferResult(@NotNull Builder builder) {
        this.errors = builder.errorsBuilder.build();
        this.transfer = builder.transfer;
    }

    /**
     * Returns transfer status flag, return value {@code true} mean that
     * transfer has been rejected.
     *
     * @return {@code true} if {@link Transfer} rejected
     */
    @Contract(pure = true)
    public boolean isRejected() {
        return !errors.isEmpty();
    }

    /**
     * Builder for {@link TransferResult}.
     */
    public static class Builder {

        private final ImmutableList.Builder<String> errorsBuilder = ImmutableList.builder();
        private Transfer transfer;

        /**
         * Creates new instance of {@link TransferResult}.
         * @return new instance of transfer result
         */
        @Contract(" -> new")
        public @NotNull TransferResult build() {
            return new TransferResult(this);
        }

        /**
         * Adds particular error description to all errors.
         * @param error error description
         * @return same builder's instance
         */
        @Contract("!null -> this; null -> fail")
        public @NotNull Builder withError(@NotNull String error) {
            this.errorsBuilder.add(error);
            return this;
        }

        /**
         * Adds collection of error descriptions to all errors.
         * @param errors error descriptions
         * @return same builder's instance
         */
        @Contract("!null -> this; null -> fail")
        public @NotNull Builder withErrors(@NotNull Collection<String> errors) {
            this.errorsBuilder.addAll(errors);
            return this;
        }

        /**
         * Adds transfer instance to future {@link TransferResult}.
         * @param transfer applied transfer
         * @return same builder's instance
         */
        @Contract("_ -> this")
        public @NotNull Builder withTransfer(Transfer transfer) {
            this.transfer = transfer;
            return this;
        }
    }
}
