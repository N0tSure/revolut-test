package com.revolut.test.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * Created on 15 Sep, 2018.
 *
 * Represents actual currency, like {@literal USD}, {@literal RUB} etc.
 *
 * @author Artemis A. Sirosh
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Currency {

    /**
     * Currency three-letter unique identifier.
     */
    @NonNull
    private String id;

    /**
     * Currency full name.
     */
    @NonNull
    private String name;

    /**
     * Creates currency's instance.
     * @param id currency identifier
     * @param name name of currency
     * @return instance of {@link Currency}
     */
    @Contract("!null, !null -> new")
    public static @NotNull Currency of(@NotNull String id,@NotNull String name) {
        return new Currency(id, name);
    }
}
