package com.revolut.test.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Created on 15 Sep, 2018.
 *
 * Represent currency {@link Currency} balance for the account.
 *
 * @author Artemis A. Sirosh
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BalanceItem {

    /**
     * Account identifier
     */
    @NonNull
    private UUID accountId;

    /**
     * Currency identifier
     */
    @NonNull
    private String currencyId;

    /**
     * Current balance value
     */
    @NonNull
    private BigDecimal value;

    /**
     * Creates instance of {@link BalanceItem}.
     * @param accountId account identifier
     * @param currencyId currency identifier
     * @param value amount of funds
     * @return new instance of {@link BalanceItem}
     */
    @Contract("!null, !null, !null -> new")
    public static @NotNull BalanceItem of(
            @NotNull UUID accountId, @NotNull String currencyId, @NotNull BigDecimal value
    ) {
        return new BalanceItem(accountId, currencyId, value);
    }
}
