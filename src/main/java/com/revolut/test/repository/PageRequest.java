package com.revolut.test.repository;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

/**
 * Created on 15 Sep, 2018.
 *
 * Implementation of {@link Page}.
 *
 * @author Artemis A. Sirosh
 */
@EqualsAndHashCode
@ToString
public class PageRequest implements Page {

    private int pageNumber;
    private int pageSize;
    private Page.Sort pageSort;

    /**
     * Creates sorted page instance using given parameters.
     * @param pageNumber number of page
     * @param pageSize size of page
     * @param sortingPropertyName name of property, which will be used for sort
     * @param sortOrder sort order direction
     * @return page instance
     */
    @NotNull
    public static Page requestSortedPage(
            int pageNumber, int pageSize,
            @NotNull String sortingPropertyName,
            @NotNull String sortOrder
    ) {
        Sort sort = new DefaultSort(
                requireNonNull(sortingPropertyName, "Sorting property name must not be null."),
                Direction.valueOf(requireNonNull(sortOrder, "Sort order direction must not be null.").toUpperCase())
        );

        return new PageRequest(pageNumber, pageSize, sort);
    }

    /**
     * Creates unsorted page instance using given parameters.
     * @param pageNumber number of page
     * @param pageSize size of page
     * @return unsorted page instance
     */
    @NotNull
    public static Page requestUnSortedPage(int pageNumber, int pageSize) {
        return new PageRequest(pageNumber, pageSize, null);
    }

    private PageRequest(int pageNumber, int pageSize, Page.Sort pageSort) {
        checkArgument(pageNumber >= 0, "Page number must be greater than zero or equal to zero");
        checkArgument(pageSize > 0, "Page size must be greater than zero.");
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.pageSort = pageSort;
    }

    @Override
    public int getSize() {
        return pageSize;
    }

    @Override
    public int getNumber() {
        return pageNumber;
    }

    @Override
    public @NotNull Optional<Sort> getSort() {
        return Optional.ofNullable(pageSort);
    }

    @EqualsAndHashCode
    @ToString
    private static class DefaultSort implements Page.Sort {

        private String propertyName;
        private Page.Direction sortDirection;

        private DefaultSort(String propertyName, Page.Direction sortDirection) {
            this.propertyName = propertyName;
            this.sortDirection = sortDirection;
        }

        @Override
        public @NotNull String getProperty() {
            return propertyName;
        }

        @Override
        public @NotNull Direction getDirection() {
            return sortDirection;
        }
    }
}
