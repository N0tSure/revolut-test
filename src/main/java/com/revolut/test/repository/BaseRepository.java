package com.revolut.test.repository;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

/**
 * Created on 15 Sep, 2018.
 *
 * Base repository which provides basic operation like save/update item and f
 * ind it.
 *
 * @param <ID> item identifier type
 * @param <T> item type
 *
 * @author Artemis A. Sirosh
 */
public interface BaseRepository<ID, T> {

    /**
     * Save or update given item.
     *
     * @param t item which will be updated
     * @return update/saved item
     */
    @NotNull
    T save(@NotNull T t);

    /**
     * Find by given identifier item.
     * @param id item's identifier
     * @return item or empty {@link Optional}
     */
    @NotNull
    Optional<T> findById(@NotNull ID id);

    /**
     * Find all available items and return paginated result.
     * @param page pagination descriptor
     * @return found items
     */
    @NotNull
    Iterable<T> findAll(@NotNull Page page);

}
