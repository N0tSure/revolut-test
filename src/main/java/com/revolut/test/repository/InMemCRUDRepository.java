package com.revolut.test.repository;

import org.jetbrains.annotations.NotNull;

import static java.util.Objects.requireNonNull;

/**
 * Created on 16 Sep, 2018.
 *
 * Implements {@link CRUDRepository} using in-memory data storage.
 *
 * @param <ID> item identifier type
 * @param <T> item type
 *
 * @author Artemis A. Sirosh
 */
abstract class InMemCRUDRepository<ID, T> extends InMemBaseRepository<ID,T> implements CRUDRepository<ID,T> {

    @Override
    public void deleteById(@NotNull ID id) {
        getInternalStorage().remove(requireNonNull(id, "id must not be null."));
    }
}
