package com.revolut.test.repository;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Created on 16 Sep, 2018.
 *
 * Implementation of a {@link BaseRepository}, which using in-memory data
 * storage.
 *
 * @param <ID> item identifier type
 * @param <T> item type
 *
 * @author Artemis A. Sirosh
 */
abstract class InMemBaseRepository<ID, T> implements BaseRepository<ID, T> {

    private final Map<ID, T> storage;

    InMemBaseRepository() {
        this.storage = new HashMap<>();
    }

    @Override
    public @NotNull Optional<T> findById(@NotNull ID id) {
        return Optional.ofNullable(storage.get(requireNonNull(id, "id must be not null.")));
    }

    /**
     * {@inheritDoc}
     * @implSpec Sorting not yet implemented.
     */
    @Override
    public @NotNull Iterable<T> findAll(@NotNull Page page) {
        final long offset = requireNonNull(page, "Page must be not null.").getSize() * page.getNumber();
        return storage.values().stream()
                .skip(offset)
                .limit(page.getSize())
                .collect(Collectors.toSet());
    }

    /**
     * Return internal storage as {@link Map}.
     * @return internal storage
     */
    Map<ID, T> getInternalStorage() {
        return storage;
    }
}
