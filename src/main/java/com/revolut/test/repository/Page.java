package com.revolut.test.repository;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

/**
 * Created on 15 Sep, 2018.
 *
 * Pagination and sorting descriptor.
 *
 * @author Artemis A. Sirosh
 */
public interface Page {

    /**
     * Page size.
     * @return page size number
     */
    int getSize();

    /**
     * Page number.
     * @return number of a page
     */
    int getNumber();

    /**
     * Page sorting, presented as {@link Sort}. If not defined items not will
     * be sorted.
     *
     * @return page sorting descriptor
     */
    @NotNull
    Optional<Sort> getSort();

    /**
     * Represents page sorting descriptor.
     */
    interface Sort {

        /**
         * Return property's name, which will be used to sort. Property must
         * implements {@link Comparable} and item must have getter method for
         * this property.
         *
         * @return property name
         */
        @NotNull
        String getProperty();

        /**
         * Returns direction of sort order.
         * @return direction of sort order
         */
        @NotNull
        Direction getDirection();

    }

    /**
     * Sort order direction, {@literal ASC} for ascending and {@literal DESC}
     * for descending.
     */
    enum Direction {
        ASC, DESC
    }
}
