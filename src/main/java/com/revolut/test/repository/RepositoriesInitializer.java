package com.revolut.test.repository;

import com.revolut.test.model.Account;
import com.revolut.test.model.BalanceItem;
import com.revolut.test.model.Currency;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * Created on 20.09.2018.
 * </p>
 *
 * Initializes test data for application.
 *
 * @author Artemis A. Sirosh
 */
public class RepositoriesInitializer {

    private final AccountRepository accountRepository;
    private final BalanceItemRepository balanceItemRepository;
    private final CurrencyRepository currencyRepository;

    @Inject
    RepositoriesInitializer(
            AccountRepository accountRepository,
            BalanceItemRepository balanceItemRepository,
            CurrencyRepository currencyRepository
    ) {
        this.accountRepository = accountRepository;
        this.balanceItemRepository = balanceItemRepository;
        this.currencyRepository = currencyRepository;
    }

    /**
     * Creates main {@link Account} with base currencies: {@literal USD},
     * {@literal RUB}, {@literal EUR} and {@literal CNY}.
     */
    public void afterDependenciesSet() {
        Account mainAccount = accountRepository.save(Account.newAccount());

        final List<Currency> currencies = Arrays.asList(
                Currency.of("USD", "United States Dollar"),
                Currency.of("RUB", "Russian Federation Rouble"),
                Currency.of("EUR", "European Union Euro"),
                Currency.of("CNY", "Chinese Yuan")
        );

        currencies.stream()
                .peek(currencyRepository::save)
                .forEach(
                        currency -> balanceItemRepository.save(
                                BalanceItem.of(
                                        mainAccount.getId(), currency.getId(), BigDecimal.valueOf(Long.MAX_VALUE)
                                )
                        )
                );



    }
}
