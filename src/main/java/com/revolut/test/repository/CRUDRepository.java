package com.revolut.test.repository;

import org.jetbrains.annotations.NotNull;

/**
 * Created on 15 Sep, 2018.
 *
 * This repository provides all operations from {@link BaseRepository} and
 * provides delete operation.
 *
 * @author Artemis A. Sirosh
 */
public interface CRUDRepository<ID, T> extends BaseRepository<ID, T> {

    /**
     * Removes item by identifier.
     * @param id item's identifier
     */
    void deleteById(@NotNull ID id);
}
