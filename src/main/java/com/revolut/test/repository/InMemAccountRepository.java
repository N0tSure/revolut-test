package com.revolut.test.repository;

import com.revolut.test.model.Account;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

import static java.util.Objects.requireNonNull;

/**
 * Created on 17 Sep, 2018.
 *
 * Implementation of {@link AccountRepository} using in-memory data storage.
 *
 * @author Artemis A. Sirosh
 */
class InMemAccountRepository extends InMemCRUDRepository<UUID, Account> implements AccountRepository {

    /**
     * {@inheritDoc}
     * @implSpec Not manage {@link com.revolut.test.model.BalanceItem}
     * instances.
     */
    @Override
    public @NotNull Account save(@NotNull Account account) {
        return getInternalStorage().merge(
                requireNonNull(account, "Account must be not null.").getId(),
                account,
                (ext, nxt) -> nxt
        );
    }
}
