package com.revolut.test.repository;

import com.revolut.test.model.Account;

import java.util.UUID;

/**
 * Created on 16 Sep, 2018.
 *
 * Provides CRUD operation for {@link Account}.
 *
 * @author Artemis A. Sirosh
 */
public interface AccountRepository extends CRUDRepository<UUID, Account> {
}
