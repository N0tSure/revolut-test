package com.revolut.test.repository;

import com.revolut.test.model.BalanceItem;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * Created on 16 Sep, 2018.
 *
 * @author Artemis A. Sirosh
 */
class InMemBalanceItemRepository extends InMemCRUDRepository<BalanceItemRepository.BalanceItemKey, BalanceItem>
        implements BalanceItemRepository {

    @Override
    public @NotNull BalanceItem save(@NotNull BalanceItem item) {
        return getInternalStorage()
                .merge(BalanceItemRepository.toBalanceItemKey(item), item, (ext, nxt) -> nxt);
    }

    @Override
    public @NotNull Iterable<BalanceItem> findByAccountId(@NotNull UUID accountId) {
        return getInternalStorage().values().stream()
                .filter(item -> item.getAccountId().equals(requireNonNull(accountId, "Account id must be not null.")))
                .collect(Collectors.toSet());
    }
}
