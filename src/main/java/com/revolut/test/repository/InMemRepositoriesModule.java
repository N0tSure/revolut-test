package com.revolut.test.repository;

import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

/**
 * <p>
 * Created on 20.09.2018.
 * </p>
 *
 * Dagger's Module which provides all repositories instances.
 *
 * @author Artemis A. Sirosh
 */
@Module
public class InMemRepositoriesModule {

    @Provides
    @Singleton
    static AccountRepository provideAccountRepository() {
        return new InMemAccountRepository();
    }

    @Provides
    @Singleton
    static BalanceItemRepository providesBalanceItemRepository() {
        return new InMemBalanceItemRepository();
    }

    @Provides
    @Singleton
    static CurrencyRepository provideCurrencyRepository() {
        return new InMemCurrencyRepository();
    }

    @Provides
    @Singleton
    static TransferRepository provideTransferRepository() {
        return new InMemTransferRepository();
    }
}
