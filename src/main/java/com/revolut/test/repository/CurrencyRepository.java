package com.revolut.test.repository;

import com.revolut.test.model.Currency;

/**
 * Created on 16 Sep, 2018.
 *
 * Provides API for saving, updating and finding {@link Currency} items.
 * Removing currency not possible for this interface because this may be
 * cause of data inconsistency.
 *
 * @author Artemis A. Sirosh
 */
public interface CurrencyRepository extends BaseRepository<String, Currency> {
}
