package com.revolut.test.repository;

import com.revolut.test.model.Transfer;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

import static java.util.Objects.requireNonNull;

/**
 * Created on 16 Sep, 2018.
 *
 * Implementation of {@link TransferRepository} which using in-memory data
 * storage.
 *
 * @author Artemis A. Sirosh
 */
class InMemTransferRepository extends InMemBaseRepository<UUID, Transfer> implements TransferRepository {

    /**
     * {@inheritDoc}
     * @implSpec Update operation for {@link Transfer} restricted.
     * @throws IllegalStateException when trying to update existed
     * {@link Transfer}.
     */
    @Override
    public @NotNull Transfer save(@NotNull Transfer transfer) {
        if (!getInternalStorage().containsKey(requireNonNull(transfer, "Transfer is null.").getId())) {
            getInternalStorage().put(transfer.getId(), transfer);
            return transfer;
        }

        throw new IllegalStateException("Transfer cannot be updated!");
    }

}
