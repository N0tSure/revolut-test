package com.revolut.test.repository;

import com.revolut.test.model.BalanceItem;
import lombok.NonNull;
import lombok.Value;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

import static java.util.Objects.requireNonNull;

/**
 * Created on 16 Sep, 2018.
 *
 * Provides CRUD operations for {@link BalanceItem}.
 *
 * @author Artemis A. Sirosh
 */
public interface BalanceItemRepository extends CRUDRepository<BalanceItemRepository.BalanceItemKey, BalanceItem> {

    /**
     * Convert given {@link BalanceItem} to key {@link BalanceItemKey}.
     * @param item balance item
     * @return key for balance item
     */
    static @NotNull BalanceItemKey toBalanceItemKey(@NotNull BalanceItem item) {
        return BalanceItemKey.of(
                requireNonNull(item, "BalanceItem must be not null.").getAccountId(),
                item.getCurrencyId()
        );
    }

    @NotNull Iterable<BalanceItem> findByAccountId(@NotNull UUID accountId);

    /**
     * Represents {@link BalanceItem} identifier. For account
     * {@link com.revolut.test.model.Account} should be only one
     * {@link BalanceItem} for particular currency
     * {@link com.revolut.test.model.Currency}.
     */
    @Value(staticConstructor = "of")
    class BalanceItemKey {

        /**
         * {@link com.revolut.test.model.Account} identifier.
         */
        @NonNull
        private UUID accountId;

        /**
         * {@link com.revolut.test.model.Currency} identifier.
         */
        @NonNull
        private String currencyId;

    }
}
