package com.revolut.test.repository;

import com.revolut.test.model.Currency;
import com.revolut.test.model.Locks;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;
import java.util.concurrent.locks.Lock;

import static java.util.Objects.requireNonNull;

/**
 * Created on 16 Sep, 2018.
 *
 * Implementation of a {@link CurrencyRepository} base on in-memory data
 * storage.
 *
 * @author Artemis A. Sirosh
 */
class InMemCurrencyRepository extends InMemBaseRepository<String, Currency> implements CurrencyRepository {

    @Override
    public @NotNull Currency save(@NotNull Currency currency) {

        final Lock lock = Locks.getCurrencyLock();
        try {

            lock.lock();
            return getInternalStorage()
                    .merge(requireNonNull(currency, "Currency is null.").getId(), currency, (ext, nxt) -> nxt);
        } finally {
            lock.unlock();
        }

    }

    @Override
    public @NotNull Optional<Currency> findById(@NotNull String id) {

        final Lock lock = Locks.getCurrencyLock();
        try {

            lock.lock();
            return super.findById(id);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public @NotNull Iterable<Currency> findAll(@NotNull Page page) {

        final Lock lock = Locks.getCurrencyLock();
        try {

            lock.lock();
            return super.findAll(page);
        } finally {
            lock.unlock();
        }
    }
}
