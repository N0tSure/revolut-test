package com.revolut.test.repository;

import com.revolut.test.model.Transfer;

import java.util.UUID;

/**
 * Created on 16 Sep, 2018.
 *
 * Provides API using that possible save new {@link Transfer} and find
 * transfers which already exists. It's not possible to update and delete
 * already existed transfers, because this is historical data.
 *
 * @author Artemis A. Sirosh
 */
public interface TransferRepository extends BaseRepository<UUID, Transfer> {
}
