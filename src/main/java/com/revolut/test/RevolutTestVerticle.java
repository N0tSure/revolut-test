package com.revolut.test;

import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.revolut.test.model.Account;
import com.revolut.test.model.Currency;
import com.revolut.test.model.Transfer;
import com.revolut.test.model.TransferResult;
import com.revolut.test.repository.Page;
import com.revolut.test.repository.PageRequest;
import com.revolut.test.service.ServiceComponent;
import com.revolut.test.service.DaggerServiceComponent;
import dagger.internal.Preconditions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

public class RevolutTestVerticle extends AbstractVerticle {

    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String APPLICATION_JSON = "application/json; charset=utf-8";

    private ServiceComponent serviceComponent;

    static {
        /*
         * Handles JDK 8 java.util.Optional
         */
        Json.mapper.registerModule(new Jdk8Module());
    }

    @Override
    public void start(final Future<Void> future) {

        this.serviceComponent = DaggerServiceComponent.create();
        this.serviceComponent.repositoriesInitializer().afterDependenciesSet();

        final Router router = Router.router(vertx);


        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response.putHeader(CONTENT_TYPE_HEADER, "text/html")
                    .end("<h1>Revolut test.</h1>");
        });

        router.route("/currencies*").handler(BodyHandler.create());
        router.post("/currencies").handler(this::addCurrency);
        router.get("/currencies/:id").handler(this::getCurrency);
        router.get("/currencies").handler(this::getCurrencies);

        router.route("/accounts*").handler(BodyHandler.create());
        router.post("/accounts").handler(this::addAccount);
        router.get("/accounts/:id").handler(this::getAccount);
        router.get("/accounts").handler(this::getAccounts);
        router.delete("/accounts/:id").handler(this::deleteAccount);

        router.route("/transfers*").handler(BodyHandler.create());
        router.post("/transfers").handler(this::performTransfer);
        router.get("/transfers/:id").handler(this::getTransfer);
        router.get("/transfers").handler(this::getTransfers);

        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(
                        config().getInteger("http.port", 8081),
                        result -> {
                            if (result.succeeded()) {
                                future.complete();
                            } else {
                                future.fail(result.cause());
                            }
                        }
                );
    }

    private void addCurrency(final RoutingContext context) {
        try {
            final  Currency currency = Json.decodeValue(context.getBodyAsString(), Currency.class);
            final Currency savedCurrency = serviceComponent.currencyRepository().save(currency);
            context.response()
                    .setStatusCode(201)
                    .putHeader(CONTENT_TYPE_HEADER, APPLICATION_JSON)
                    .end(Json.encode(savedCurrency));

        } catch (DecodeException exc) {
            context.response().setStatusCode(400).end();
        }
    }

    private void getCurrency(final RoutingContext context) {
        final String id = context.request().getParam("id");
        if (id == null) {
            context.response().setStatusCode(400);
        } else {
            final Optional<Currency> maybeCurrency = serviceComponent.currencyRepository().findById(id);
            if (maybeCurrency.isPresent()) {
                context.response()
                        .setStatusCode(200)
                        .putHeader(CONTENT_TYPE_HEADER, APPLICATION_JSON)
                        .end(Json.encode(maybeCurrency.get()));
            } else {
                context.response().setStatusCode(404).end();
            }
        }
    }

    private void getCurrencies(final RoutingContext context) {
        final MultiMap queryParams = context.queryParams();
        if (queryParams.contains("num") && queryParams.contains("size")) {
            Page page = PageRequest.requestUnSortedPage(
                    new Integer(queryParams.get("num")),
                    new Integer(queryParams.get("size"))
            );

            context.response()
                    .setStatusCode(200)
                    .putHeader(CONTENT_TYPE_HEADER, APPLICATION_JSON)
                    .end(Json.encode(serviceComponent.currencyRepository().findAll(page)));
        } else {
            context.response().setStatusCode(400).end();
        }
    }

    private void addAccount(final RoutingContext context) {
        final Account account = serviceComponent.accountService().save(Account.newAccount());

        context.response().setStatusCode(201)
                .putHeader(CONTENT_TYPE_HEADER, APPLICATION_JSON)
                .end(Json.encode(account));
    }

    private void getAccount(final RoutingContext context) {
        final String id = context.request().getParam("id");
        try {
            Optional<Account> maybeAccount = serviceComponent.accountService().findById(UUID.fromString(id));
            if (maybeAccount.isPresent()) {
                context.response()
                        .setStatusCode(200)
                        .putHeader(CONTENT_TYPE_HEADER, APPLICATION_JSON)
                        .end(Json.encode(maybeAccount.get()));
            } else {
                context.response().setStatusCode(404).end();
            }
        } catch (IllegalArgumentException | NullPointerException exc) {
            context.response().setStatusCode(400).end();
        }
    }

    private void getAccounts(final RoutingContext context) {
        final MultiMap queryParams = context.queryParams();
        if (queryParams.contains("num") && queryParams.contains("size")) {
            Page page = PageRequest.requestUnSortedPage(
                    new Integer(queryParams.get("num")),
                    new Integer(queryParams.get("size"))
            );

            context.response()
                    .setStatusCode(200)
                    .putHeader(CONTENT_TYPE_HEADER, APPLICATION_JSON)
                    .end(Json.encode(serviceComponent.accountService().findAll(page)));
        } else {
            context.response().setStatusCode(400).end();
        }
    }

    private void deleteAccount(final RoutingContext context) {

        try {
            UUID id = UUID.fromString(context.request().getParam("id"));
            serviceComponent.accountService().deleteById(id);

            context.response().setStatusCode(204).end();
        } catch (IllegalArgumentException | NullPointerException exc) {
            context.response().setStatusCode(400).end();
        }
    }

    private void performTransfer(final RoutingContext context) {

        try {
            final JsonObject transferObject = context.getBodyAsJson();
            final UUID sendersAccountId = UUID.fromString(transferObject.getString("senderAccountId"));
            final UUID receiverAccountId = UUID.fromString(transferObject.getString("receiverAccountId"));
            final String currencyId = Preconditions.checkNotNull(transferObject.getString("currencyId"));
            final BigDecimal funds = new BigDecimal(transferObject.getString("funds"));
            final Transfer transfer = Transfer.newTransfer(sendersAccountId, receiverAccountId, currencyId, funds);
            TransferResult result = serviceComponent.transferService().perform(transfer);
            if (!result.isRejected()) {
                context.response()
                        .setStatusCode(201)
                        .putHeader(CONTENT_TYPE_HEADER, APPLICATION_JSON)
                        .end(Json.encode(result.getTransfer()));
            } else {
                context.response()
                        .setStatusCode(400)
                        .putHeader(CONTENT_TYPE_HEADER, APPLICATION_JSON)
                        .end(Json.encode(result.getErrors()));
            }
        } catch (IllegalArgumentException | NullPointerException exc) {
            context.response().setStatusCode(400).end();
        }
    }

    private void getTransfer(final RoutingContext context) {
        try {
            UUID id = UUID.fromString(context.request().getParam("id"));
            Optional<Transfer> maybeTransfer = serviceComponent.transferService().findById(id);
            if (maybeTransfer.isPresent()) {
                context.response()
                        .setStatusCode(200)
                        .putHeader(CONTENT_TYPE_HEADER, APPLICATION_JSON)
                        .end(Json.encode(maybeTransfer.get()));
            } else {
                context.response().setStatusCode(404).end();
            }

        } catch (IllegalArgumentException | NullPointerException exc) {
            context.response().setStatusCode(400).end();
        }
    }

    private void getTransfers(final RoutingContext context) {
        final MultiMap queryParams = context.queryParams();
        if (queryParams.contains("num") && queryParams.contains("size")) {
            Page page = PageRequest.requestUnSortedPage(
                    new Integer(queryParams.get("num")),
                    new Integer(queryParams.get("size"))
            );

            context.response()
                    .setStatusCode(200)
                    .putHeader(CONTENT_TYPE_HEADER, APPLICATION_JSON)
                    .end(Json.encode(serviceComponent.transferService().findAll(page)));
        } else {
            context.response().setStatusCode(400).end();
        }
    }
    
}