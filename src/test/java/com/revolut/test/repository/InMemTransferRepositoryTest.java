package com.revolut.test.repository;

import com.revolut.test.model.Transfer;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created on 16 Sep, 2018.
 *
 * @author Artemis A. Sirosh
 */
public class InMemTransferRepositoryTest {

    private TransferRepository transferRepository;


    @Before
    public void setUp() {
        transferRepository = new InMemTransferRepository();
        Stream.generate(randomTransferSupplier())
                .limit(10)
                .forEach(transfer -> transferRepository.save(transfer));
    }

    @Test
    public void shouldSaveAndFindTransfer() {

        Transfer expectedTransfer = transferRepository.save(randomTransferSupplier().get());
        assertEquals(expectedTransfer, transferRepository.findById(expectedTransfer.getId()).orElse(null));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldPreventTransferUpdating() {
        Transfer transfer = transferRepository.save(randomTransferSupplier().get());

        transfer.setCurrencyId("foo");
        transferRepository.save(transfer);
    }

    @Test
    public void shouldFindOneTransferAmongOthers() {
        Transfer transfer = transferRepository.save(randomTransferSupplier().get());
        assertEquals(transfer, transferRepository.findById(transfer.getId()).orElse(null));
    }

    @Test
    public void shouldReturnProperPaginatedTransfers() {
        Set<Transfer> transfers = StreamSupport.stream(
                transferRepository.findAll(PageRequest.requestUnSortedPage(1, 6)).spliterator(),
                false
        ).collect(Collectors.toSet());

        assertThat(transfers, hasSize(4));
    }

    private static Supplier<Transfer> randomTransferSupplier() {
        final String[] currenciesIds = { "USD", "RUB", "EUR", "CNY" };
        return () -> {
            final BigDecimal randFunds = new BigDecimal(Math.random() * 1000);
            final String currency = currenciesIds[(int) Math.floor(Math.random() * currenciesIds.length)];
            return Transfer.newTransfer(UUID.randomUUID(), UUID.randomUUID(), currency, randFunds);
        };
    }
}
