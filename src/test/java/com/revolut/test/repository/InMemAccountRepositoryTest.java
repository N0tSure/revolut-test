package com.revolut.test.repository;

import com.revolut.test.model.Account;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

/**
 * Created on 17 Sep, 2018.
 *
 * @author Artemis A. Sirosh
 */
public class InMemAccountRepositoryTest {

    private AccountRepository repository;

    @Before
    public void setUp() {
        this.repository = new InMemAccountRepository();
        Stream.generate(Account::newAccount)
                .limit(10)
                .forEach(account -> repository.save(account));
    }

    @Test
    public void shouldSaveAndFindNewAccount() {
        Account account = repository.save(Account.newAccount());

        assertThat(repository.findById(account.getId()).orElse(null), equalTo(account));
    }

    @Test
    public void shouldReturnProperPage() {
        Set<Account> accounts = StreamSupport.stream(
                repository.findAll(PageRequest.requestUnSortedPage(3, 3)).spliterator(),
                false
        ).collect(Collectors.toSet());

        assertThat(accounts, hasSize(1));
    }

    @Test
    public void shouldRemoveAccount() {
        Account account = repository.save(Account.newAccount());
        repository.deleteById(account.getId());

        assertFalse(repository.findById(account.getId()).isPresent());
    }
}
