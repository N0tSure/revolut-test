package com.revolut.test.repository;

import com.revolut.test.model.BalanceItem;
import com.revolut.test.repository.BalanceItemRepository.BalanceItemKey;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.revolut.test.repository.BalanceItemRepository.toBalanceItemKey;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

/**
 * Created on 16 Sep, 2018.
 *
 * @author Artemis A. Sirosh
 */
public class InMemBalanceItemRepositoryTest {

    private BalanceItemRepository repository;

    @Before
    public void setUp() {
        repository = new InMemBalanceItemRepository();
        Stream.generate(randomBalanceItemSupplier())
                .limit(10)
                .forEach(item -> repository.save(item));
    }

    @Test
    public void shouldSaveAndFindItem() {
        BalanceItem expectedItem = repository.save(randomBalanceItemSupplier().get());

        assertThat(repository.findById(toBalanceItemKey(expectedItem)).orElse(null), equalTo(expectedItem));
    }

    @Test
    public void shouldUpdateExistedItem() {

        BalanceItem expectedItem = repository.save(randomBalanceItemSupplier().get());
        expectedItem.setValue(expectedItem.getValue().add(new BigDecimal(1000.0)));
        repository.save(expectedItem);

        assertThat(repository.findById(toBalanceItemKey(expectedItem)).orElse(null), equalTo(expectedItem));

    }

    @Test
    public void shouldReturnPaginatedItems() {
        List<BalanceItem> items = StreamSupport.stream(
                repository.findAll(PageRequest.requestUnSortedPage(1, 4)).spliterator(),
                false
        ).collect(Collectors.toList());

        assertThat(items, hasSize(4));
    }

    @Test
    public void shouldDeleteItem() {
        BalanceItem item = repository.save(randomBalanceItemSupplier().get());
        repository.deleteById(BalanceItemKey.of(item.getAccountId(), item.getCurrencyId()));

        assertFalse(repository.findById(toBalanceItemKey(item)).isPresent());
    }

    @Test
    public void shouldFindByItemsByAccountId() {
        final UUID accountId = UUID.randomUUID();
        final List<BalanceItem> expectedItems = Arrays.asList(
                BalanceItem.of(accountId, "USD", new BigDecimal(100)),
                BalanceItem.of(accountId, "RUB", new BigDecimal(100)),
                BalanceItem.of(accountId, "EUR", new BigDecimal(100))
        );

        expectedItems.forEach(item -> repository.save(item));
        Set<BalanceItem> actualItems = StreamSupport
                .stream(repository.findByAccountId(accountId).spliterator(), false)
                .collect(Collectors.toSet());

        assertThat(actualItems, containsInAnyOrder(expectedItems.toArray()));
    }

    private static Supplier<BalanceItem> randomBalanceItemSupplier() {
        final String[] currencyIds = { "USD", "RUB", "EUR", "CNY" };
        return () -> {
            final BigDecimal randFunds = new BigDecimal(Math.random() * 1000);
            final String currencyId = currencyIds[(int) Math.floor(Math.random() * currencyIds.length)];
            return BalanceItem.of(UUID.randomUUID(), currencyId, randFunds);
        };
    }
}
