package com.revolut.test.repository;

import com.revolut.test.model.Currency;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created on 16 Sep, 2018.
 *
 * @author Artemis A. Sirosh
 */
@RunWith(JUnit4.class)
public class InMemCurrencyRepositoryTest {

    private CurrencyRepository currencyRepository;

    @Before
    public void setUp() {
        this.currencyRepository = new InMemCurrencyRepository();
        currencyRepository.save(Currency.of("USD", "United States Dollar"));
        currencyRepository.save(Currency.of("RUB", "Russian Federation Rouble"));
        currencyRepository.save(Currency.of("EUR", "European Union Euro"));
    }

    @Test
    public void shouldSaveAndFindCurrency() {

        Currency yuan = Currency.of("CNY", "Republic of China Yuan");
        Currency actualCurrency = currencyRepository.save(yuan);
        assertEquals(yuan, actualCurrency);

        Optional<Currency> currencyOptional = currencyRepository.findById("CNY");

        assertTrue(currencyOptional.isPresent());
        assertEquals(yuan, currencyOptional.get());
    }

    @Test
    public void shouldUpdateCurrencyName() {

        Currency dollar = Currency.of("USD", "Foo");
        currencyRepository.save(dollar);

        Optional<Currency> actualCurrencyOptional = currencyRepository.findById(dollar.getId());

        assertTrue(actualCurrencyOptional.isPresent());
        assertThat(actualCurrencyOptional.get().getName(), equalTo("Foo"));
    }

    @Test
    public void shouldFindOneCurrencyAmongSeveralOthers() {

        Optional<Currency> actualOpt = currencyRepository.findById("USD");

        assertTrue(actualOpt.isPresent());
        assertThat(actualOpt.get(), equalTo(Currency.of("USD", "United States Dollar")));
    }

    @Test
    public void shouldReturnCorrectPage() {

        Set<Currency> currencies = StreamSupport.stream(
                currencyRepository.findAll(PageRequest.requestUnSortedPage(0, 2)).spliterator(),
                false
        ).collect(Collectors.toSet());

        assertThat(currencies, Matchers.hasSize(2));
    }
}
