package com.revolut.test.repository;

import dagger.Component;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Singleton;

import static org.junit.Assert.assertNotNull;

/**
 * <p>
 * Created on 20.09.2018.
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class InMemRepositoryModuleTest {

    @Test
    public void shouldInitializeRepositories() {
        TestComponent testComponent = DaggerInMemRepositoryModuleTest_TestComponent.create();

        assertNotNull("AccountRepository not initialized", testComponent.accountRepository());
        assertNotNull("BalanceItemRepository not initialized", testComponent.balanceItemRepository());
        assertNotNull("CurrencyRepository not initialized", testComponent.currencyRepository());
        assertNotNull("TransferRepository not initialized", testComponent.transferRepository());
    }

    @Singleton
    @Component(modules = {InMemRepositoriesModule.class})
    interface TestComponent {

        AccountRepository accountRepository();
        BalanceItemRepository balanceItemRepository();
        CurrencyRepository currencyRepository();
        TransferRepository transferRepository();
    }
}
