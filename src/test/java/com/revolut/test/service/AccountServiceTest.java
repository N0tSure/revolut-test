package com.revolut.test.service;

import com.revolut.test.model.Account;
import com.revolut.test.model.BalanceItem;
import com.revolut.test.repository.AccountRepository;
import com.revolut.test.repository.BalanceItemRepository;
import com.revolut.test.repository.BalanceItemRepository.BalanceItemKey;
import com.revolut.test.repository.Page;
import com.revolut.test.repository.PageRequest;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.AdditionalMatchers.find;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

/**
 * <p>
 * Created on 17.09.2018.
 * </p>
 *
 * @author Artemis A. Sirosh
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private BalanceItemRepository balanceItemRepository;

    private AccountService accountService;

    @Before
    public void setUp() {
        this.accountService = new AccountService(accountRepository, balanceItemRepository);
    }

    @Test
    public void shouldFindAndBuildProperAccount() {
        Account account = Account.newAccount();
        account.setBalanceItems(
                Collections.singleton(BalanceItem.of(account.getId(), "USD", new BigDecimal(100.0)))
        );

        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        when(balanceItemRepository.findByAccountId(account.getId())).thenReturn(account.getBalanceItems());

        Optional<Account> actualAccount = accountService.findById(account.getId());

        verify(accountRepository, times(1)).findById(account.getId());
        verify(balanceItemRepository, times(1)).findByAccountId(account.getId());

        assertEquals(account, actualAccount.orElse(null));
    }

    @Test
    public void shouldDeleteAccountById() {
        Account account = Account.newAccount();
        List<BalanceItem> items = Arrays.asList(
                BalanceItem.of(account.getId(), "USD", new BigDecimal(100.0)),
                BalanceItem.of(account.getId(), "RUB", new BigDecimal(1000.0))
        );
        account.setBalanceItems(new HashSet<>(items));

        when(balanceItemRepository.findByAccountId(account.getId())).thenReturn(account.getBalanceItems());

        accountService.deleteById(account.getId());

        verify(balanceItemRepository, times(1)).findByAccountId(account.getId());
        verify(balanceItemRepository, times(2)).deleteById(
                        or(
                                eq(BalanceItemRepository.toBalanceItemKey(items.get(0))),
                                eq(BalanceItemRepository.toBalanceItemKey(items.get(1)))
                        )
        );

        verify(accountRepository, times(1)).deleteById(account.getId());
    }

    @Test
    public void shouldSaveNewAccount() {
        Account account = Account.newAccount();
        when(accountRepository.save(account))
                .thenReturn(Account.existsAccount(account.getId(), account.getBalanceItems()));

        assertEquals(account, accountService.save(account));
        verify(accountRepository, times(1)).save(account);
    }

    @Test
    public void shouldRemoveBalanceItemAndUpdateOther() {
        Account existedAccount = Account.newAccount();
        List<BalanceItem> existedItems = Arrays.asList(
                BalanceItem.of(existedAccount.getId(), "USD", new BigDecimal(100.0)),
                BalanceItem.of(existedAccount.getId(), "RUB", new BigDecimal(1000.0))
        );

        Account updatedAccount = Account.existsAccount(existedAccount.getId(),
                Collections.singletonList(
                        BalanceItem.of(existedAccount.getId(), "USD", new BigDecimal(110.0))
                )
        );

        when(accountRepository.save(updatedAccount)).thenReturn(updatedAccount);
        when(balanceItemRepository.findByAccountId(updatedAccount.getId())).thenReturn(existedItems);
        when(balanceItemRepository.save(any()))
                .thenReturn(BalanceItem.of(updatedAccount.getId(), "USD", new BigDecimal(110.0)));

        Account actualAccount = accountService.save(updatedAccount);

        verify(accountRepository).save(updatedAccount);
        verify(balanceItemRepository).findByAccountId(updatedAccount.getId());
        verify(balanceItemRepository, times(1))
                .deleteById(BalanceItemKey.of(updatedAccount.getId(), "RUB"));

        verify(balanceItemRepository, times(1))
                .save(BalanceItem.of(updatedAccount.getId(), "USD", new BigDecimal(110.0)));

        assertEquals(updatedAccount, actualAccount);
    }

    @Test
    public void shouldReturnProperPage() {
        Page page = PageRequest.requestUnSortedPage(0, 1);
        Account expectedAccount = Account.newAccount();
        List<BalanceItem> items = Arrays.asList(
                BalanceItem.of(expectedAccount.getId(), "USD", new BigDecimal(100.0)),
                BalanceItem.of(expectedAccount.getId(), "RUB", new BigDecimal(1000.0))
        );

        when(accountRepository.findAll(page)).thenReturn(Collections.singletonList(expectedAccount));
        when(balanceItemRepository.findByAccountId(expectedAccount.getId())).thenReturn(items);

        Account actualAccount = StreamSupport.stream(accountService.findAll(page).spliterator(), false)
                .findFirst()
                .orElse(null);

        verify(accountRepository, times(1)).findAll(page);
        verify(balanceItemRepository).findByAccountId(expectedAccount.getId());

        assertNotNull(actualAccount);
        assertEquals(expectedAccount.getId(), actualAccount.getId());
        assertThat(actualAccount.getBalanceItems(), Matchers.containsInAnyOrder(items.toArray()));
    }
}
