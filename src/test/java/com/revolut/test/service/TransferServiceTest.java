package com.revolut.test.service;

import com.revolut.test.model.*;
import com.revolut.test.model.Currency;
import com.revolut.test.repository.*;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.singleton;
import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.*;

/**
 * <p>
 * Created on 18.09.2018.
 * </p>
 *
 * Test checklist:
 * 1) Should perform transfer for amount of currency, which not bigger than
 *    current account balance to existed receiver account {@link BalanceItem}:
 *    {@link #shouldTransferNotAllCurrencyToExistedBalanceItem()}
 *
 * 2) Should perform transfer for amount of currency, which not equal to
 *    current account balance to existed receiver account {@link BalanceItem}:
 *    {@link #shouldTransferAllSendersCurrencyToExistedBalanceItem()}
 *
 * 3) Should perform transfer to not existed receiver account
 *    {@link BalanceItem}:
 *    {@link #shouldTransferAllSendersCurrencyToExistedBalanceItem()}
 *
 * 4) Should reject transfer from not existed account:
 *    {@link #shouldRejectTransferIfSenderAccountNotExists()}
 *
 * 5) Should reject transfer to not existed account:
 *    {@link #shouldRejectTransferToNotExistedReceiver()}
 *
 * 6) Should reject transfer for not existed currency:
 *    {@link #shouldRejectTransferForNotExistedCurrency()}
 *
 * 7) Should reject transfer for funds more than sender account has:
 *    {@link #shouldRejectTransferMoreThanSenderBalance()}
 *
 * 8) Should find one {@link Transfer} by identifier:
 * {@link #shouldFindATransferById()}
 *
 * 9) Should return proper page: {@link #shouldRequestPage()}
 *
 * @author Artemis A. Sirosh
 */
@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

    private final static Currency DOLLAR = Currency.of("USD", "United States Dollar");
    private final static Currency ROUBLE = Currency.of("RUB", "Russian Federation Rouble");

    @Mock
    private CurrencyRepository currencyRepository;

    @Mock
    private AccountService accountService;

    @Mock
    private TransferRepository transferRepository;

    private TransferService transferService;

    @Before
    public void setUp() {
        this.transferService = new TransferService(accountService, currencyRepository, transferRepository);
    }

    @Test
    public void shouldTransferNotAllCurrencyToExistedBalanceItem() {
        Account senderAccount = Account.newAccount();
        senderAccount.setBalanceItems(singleton(
                BalanceItem.of(senderAccount.getId(), DOLLAR.getId(), new BigDecimal(10.0))
        ));

        Account receiverAccount = Account.newAccount();
        receiverAccount.setBalanceItems(singleton(
                BalanceItem.of(receiverAccount.getId(), DOLLAR.getId(), new BigDecimal(10.0))
        ));

        Transfer transfer = Transfer.newTransfer(
                senderAccount.getId(),
                receiverAccount.getId(),
                DOLLAR.getId(),
                new BigDecimal(9.0)
        );

        when(currencyRepository.findById(DOLLAR.getId())).thenReturn(Optional.of(DOLLAR));
        when(accountService.findById(senderAccount.getId())).thenReturn(Optional.of(senderAccount));
        when(accountService.findById(receiverAccount.getId())).thenReturn(Optional.of(receiverAccount));
        when(transferRepository.save(notNull())).thenReturn(transfer);

        TransferResult result = transferService.perform(transfer);
        assertFalse(result.isRejected());

        verify(currencyRepository, atLeastOnce()).findById(DOLLAR.getId());
        verify(accountService, atLeastOnce()).findById(senderAccount.getId());
        verify(accountService, atLeastOnce()).findById(receiverAccount.getId());
        verify(transferRepository).save(notNull());

        verify(accountService).save(Account.existsAccount(
                receiverAccount.getId(),
                singleton(BalanceItem.of(receiverAccount.getId(), DOLLAR.getId(), new BigDecimal(19.0)))
        ));

        verify(accountService).save(Account.existsAccount(
                senderAccount.getId(),
                singleton(BalanceItem.of(senderAccount.getId(), DOLLAR.getId(), new BigDecimal(1.0)))
        ));

    }

    @Test
    public void shouldTransferAllSendersCurrencyToExistedBalanceItem() {
        Account senderAccount = Account.newAccount();
        senderAccount.setBalanceItems(Stream.of(
                BalanceItem.of(senderAccount.getId(), DOLLAR.getId(), new BigDecimal(10.0)),
                BalanceItem.of(senderAccount.getId(), ROUBLE.getId(), new BigDecimal(100.0))
        ).collect(Collectors.toSet()));

        Account receiverAccount = Account.newAccount();
        receiverAccount.setBalanceItems(
                singleton(BalanceItem.of(receiverAccount.getId(), DOLLAR.getId(), new BigDecimal(10.0)))
        );

        Transfer transfer = Transfer.newTransfer(
                senderAccount.getId(),
                receiverAccount.getId(),
                DOLLAR.getId(),
                new BigDecimal(10.0)
        );

        when(currencyRepository.findById(DOLLAR.getId())).thenReturn(Optional.of(DOLLAR));
        when(accountService.findById(senderAccount.getId())).thenReturn(Optional.of(senderAccount));
        when(accountService.findById(receiverAccount.getId())).thenReturn(Optional.of(receiverAccount));
        when(transferRepository.save(notNull())).thenReturn(transfer);

        TransferResult result = transferService.perform(transfer);
        assertFalse(result.isRejected());

        verify(currencyRepository, atLeastOnce()).findById(DOLLAR.getId());
        verify(accountService, atLeastOnce()).findById(senderAccount.getId());
        verify(accountService, atLeastOnce()).findById(receiverAccount.getId());
        verify(transferRepository).save(notNull());

        verify(accountService).save(Account.existsAccount(
                receiverAccount.getId(),
                singleton(BalanceItem.of(receiverAccount.getId(), DOLLAR.getId(), new BigDecimal(20.0)))
        ));

        verify(accountService).save(Account.existsAccount(
                senderAccount.getId(),
                singleton(BalanceItem.of(senderAccount.getId(), ROUBLE.getId(), new BigDecimal(100.0)))
        ));
    }

    @Test
    public void shouldTransferCurrencyToNotExistedReceiverBalanceItem() {
        Account senderAccount = Account.newAccount();
        senderAccount.setBalanceItems(Stream.of(
                BalanceItem.of(senderAccount.getId(), DOLLAR.getId(), new BigDecimal(10.0)),
                BalanceItem.of(senderAccount.getId(), ROUBLE.getId(), new BigDecimal(100.0))
        ).collect(Collectors.toSet()));

        Account receiverAccount = Account.newAccount();
        receiverAccount.setBalanceItems(
                singleton(BalanceItem.of(receiverAccount.getId(), ROUBLE.getId(), new BigDecimal(100.0)))
        );

        Transfer transfer = Transfer.newTransfer(
                senderAccount.getId(),
                receiverAccount.getId(),
                DOLLAR.getId(),
                new BigDecimal(10.0)
        );

        when(currencyRepository.findById(DOLLAR.getId())).thenReturn(Optional.of(DOLLAR));
        when(accountService.findById(senderAccount.getId())).thenReturn(Optional.of(senderAccount));
        when(accountService.findById(receiverAccount.getId())).thenReturn(Optional.of(receiverAccount));
        when(transferRepository.save(notNull())).thenReturn(transfer);

        TransferResult result = transferService.perform(transfer);
        assertFalse(result.isRejected());

        verify(currencyRepository,atLeastOnce()).findById(DOLLAR.getId());
        verify(accountService, atLeastOnce()).findById(senderAccount.getId());
        verify(accountService, atLeastOnce()).findById(receiverAccount.getId());
        verify(transferRepository).save(notNull());

        verify(accountService).save(Account.existsAccount(
                receiverAccount.getId(),
                Stream.of(
                        BalanceItem.of(receiverAccount.getId(), DOLLAR.getId(), new BigDecimal(10.0)),
                        BalanceItem.of(receiverAccount.getId(), ROUBLE.getId(), new BigDecimal(100.0))
                ).collect(Collectors.toSet())
        ));

        verify(accountService).save(Account.existsAccount(
                senderAccount.getId(),
                singleton(BalanceItem.of(senderAccount.getId(), ROUBLE.getId(), new BigDecimal(100.0)))
        ));
    }

    @Test
    public void shouldRejectTransferIfSenderAccountNotExists() {
        UUID notExistedAccountId = UUID.randomUUID();

        when(accountService.findById(notExistedAccountId)).thenReturn(Optional.empty());

        TransferResult result = transferService.perform(
                Transfer.newTransfer(notExistedAccountId, UUID.randomUUID(), DOLLAR.getId(), new BigDecimal(1))
        );

        assertTrue(result.isRejected());
        assertThat(result.getErrors().get(0), Matchers.startsWith("Account not existed"));
    }

    @Test
    public void shouldRejectTransferToNotExistedReceiver() {
        Account senderAccount = Account.newAccount();
        UUID notExistedReceiverAccountId = UUID.randomUUID();

        when(accountService.findById(senderAccount.getId())).thenReturn(Optional.of(senderAccount));
        when(accountService.findById(notExistedReceiverAccountId)).thenReturn(Optional.empty());

        TransferResult result = transferService.perform(
                Transfer.newTransfer(
                        senderAccount.getId(), notExistedReceiverAccountId, DOLLAR.getId(), new BigDecimal(1)
                )
        );

        assertTrue(result.isRejected());
        assertThat(result.getErrors().get(0), Matchers.startsWith("Account not existed"));
    }

    @Test
    public void shouldRejectTransferForNotExistedCurrency() {
        Account senderAccount = Account.newAccount();
        Account receiverAccount = Account.newAccount();

        when(accountService.findById(senderAccount.getId())).thenReturn(Optional.of(senderAccount));
        when(accountService.findById(receiverAccount.getId())).thenReturn(Optional.of(receiverAccount));
        when(currencyRepository.findById("FOO")).thenReturn(Optional.empty());

        TransferResult result = transferService.perform(
                Transfer.newTransfer(
                        senderAccount.getId(), receiverAccount.getId(), "FOO", new BigDecimal(10)
                )
        );

        assertTrue(result.isRejected());
        assertThat(result.getErrors().get(0), Matchers.startsWith("Currency not existed"));
    }

    @Test
    public void shouldRejectTransferMoreThanSenderBalance() {
        Account senderAccount = Account.newAccount();
        Account receiverAccount = Account.newAccount();

        when(accountService.findById(senderAccount.getId())).thenReturn(Optional.of(senderAccount));
        when(accountService.findById(receiverAccount.getId())).thenReturn(Optional.of(receiverAccount));
        when(currencyRepository.findById(DOLLAR.getId())).thenReturn(Optional.of(DOLLAR));

        TransferResult result = transferService.perform(
                Transfer.newTransfer(
                        senderAccount.getId(), receiverAccount.getId(), DOLLAR.getId(), new BigDecimal(10)
                )
        );

        assertTrue(result.isRejected());
        assertThat(result.getErrors().get(0), Matchers.startsWith("Insufficient funds"));
    }

    @Test
    public void shouldFindATransferById() {
        Transfer expectedTransfer = Transfer.newTransfer(
                UUID.randomUUID(), UUID.randomUUID(), DOLLAR.getId(), BigDecimal.ONE
        );

        when(transferRepository.findById(expectedTransfer.getId()))
                .thenReturn(Optional.of(expectedTransfer));

        Transfer transfer = transferService.findById(expectedTransfer.getId()).orElse(null);
        assertEquals(expectedTransfer, transfer);

        verify(transferRepository).findById(expectedTransfer.getId());
    }

    @Test
    public void shouldRequestPage() {
        Page page = PageRequest.requestUnSortedPage(0, 5);

        when(transferRepository.findAll(page)).thenReturn(Collections.emptyList());

        Iterable<Transfer> actualPage = transferService.findAll(page);
        assertThat(actualPage, emptyIterable());

        verify(transferRepository).findAll(page);
    }
}
