package com.revolut.test.service;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * <p>
 * Created on 20.09.2018.
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class ServiceComponentTest {

    @Test
    public void shouldInstantiateServicesProperly() {
        ServiceComponent component = DaggerServiceComponent.create();

        assertNotNull("AccountService is null", component.accountService());
        assertNotNull("TransferService is null", component.transferService());
    }
}
