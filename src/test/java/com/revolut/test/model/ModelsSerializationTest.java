package com.revolut.test.model;

import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import io.vertx.core.json.Json;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.UUID;

import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

/**
 * <p>
 * Created on 20.09.2018.
 * </p>
 *
 * @author Artemis A. Sirosh
 */
public class ModelsSerializationTest {

    @BeforeClass
    public static void registerJacksonModule() {
        Json.mapper.registerModule(new Jdk8Module());
    }

    @Test
    public void currencySerializationTest() {
        String serialCurrency = Json.encode(Currency.of("USD", "Dollar"));

        Currency currency = Json.decodeValue(serialCurrency, Currency.class);
        assertThat(currency.getId(), equalTo("USD"));
    }

    @Test
    public void balanceItemSerializationTest() {
        String serial = Json.encode(BalanceItem.of(UUID.randomUUID(), "USD", BigDecimal.ZERO));

        BalanceItem item = Json.decodeValue(serial, BalanceItem.class);
        assertThat(item.getAccountId(), notNullValue());
        assertThat(item.getCurrencyId(), equalTo("USD"));
        assertThat(item.getValue(), comparesEqualTo(BigDecimal.ZERO));
    }

    @Test
    public void accountSerializationTest() {
        Account expectedAccount = Account.existsAccount(
                UUID.randomUUID(),
                Collections.singleton(BalanceItem.of(UUID.randomUUID(), "USD", BigDecimal.ZERO))
        );
        String serial = Json.encode(expectedAccount);

        Account actualAccount = Json.decodeValue(serial, Account.class);
        assertThat(actualAccount, equalTo(expectedAccount));
    }

    @Test
    public void transferSerializationTest() {

        String serial = Json.encode(
                Transfer.newTransfer(UUID.randomUUID(), UUID.randomUUID(), "USD", BigDecimal.ONE)
        );

        Transfer actual = Json.decodeValue(serial, Transfer.class);
        assertThat(actual.getId(), notNullValue());
        assertThat(actual.getSenderAccountId(), notNullValue());
        assertThat(actual.getReceiverAccountId(), notNullValue());
        assertThat(actual.getCurrencyId(), equalTo("USD"));
        assertThat(actual.getFunds(), comparesEqualTo(BigDecimal.ONE));
        assertFalse(actual.getAppliedAt().isPresent());
    }
}
