# Revolut Test

Simple application, provides operation with accounts, currencies and transfer.
Technology stack:

- Java 8
- Gradle
- Dagger 2
- Vert.x

## Test, build and execute

To run auto tests:

`./gradlew test`

Build `JAR` file with Shadow Plugin:

`./gradlew shadowJar`

Run `JAR` file:

`java -jar build\libs\revolut-test-0.1.0-SNAPSHOT.jar`

## Domain model

### Currency

Have identifier (`id`) and name (`name`):

```json
{
  "id": "EUR",
  "name": "European Union Euro"
}
```

To create new currency should be provided identifier and name:

```
curl -X POST -H 'Content-Type: application/json' -i 'http://localhost:8080/currencies' --data '{
  "id": "EUR",
  "name": "European Union Euro"
}'
```

Particular currency may be obtained by `id`:

```
curl -X GET -H 'Content-Type: application/json' -i 'http://localhost:8080/currencies/USD' --data '{
  "id": "EUR",
  "name": "European Union Euro"
}'
``` 

Several currencies requests using pagination, parameters `num` and `size` for page 
number and size respectively should be used:

```
curl -X GET -H 'Content-Type: application/json' -i 'http://localhost:8080/currencies?num=0&size=10' --data '{
  "id": "EUR",
  "name": "European Union Euro"
}'
```

Deleting for currencies not supported.

### Account

Have identifier (`id`) in form of UUID and set of balance item, which contains
amount of funds for particular currency.

```json
{
  "id": "c154a309-9e25-4e8b-8e25-ef6ce291bfe1",
  "balanceItems": [{
    "accountId": "c154a309-9e25-4e8b-8e25-ef6ce291bfe1",
    "currencyId": "USD",
    "value": 100
  }]
}
```

New account creates by HTTP `POST` request without body (if body will be 
presented, it's be rejected):
 
```
curl -X POST -H 'Content-Type: application/json' -i 'http://localhost:8080/accounts' --data '{
  "id": "EUR",
  "name": "European Union Euro"
}'
```

Delete operation applied by Account's identifier:

```
curl -X DELETE -H 'Content-Type: application/json' -i 'http://localhost:8080/accounts/90bb7f3d-02a1-4c3e-be15-a6e83313fc78' --data '{
  "id": "EUR",
  "name": "European Union Euro"
}'
```

If deletion be successful `204` HTTP status will be returned.

Requesting existed accounts same as for `Currency`. 

### Transfer

Represent transfer of a funds between two account in particular currency. Transfer 
contains own id, both identifiers of sender account and receiver, identifier of currency, 
funds for transfer and date/time of when transfer will be applied.

```json
{
  "id": "efb6062f-be1d-4c8e-b902-c30c5b63749a",
  "senderAccountId": "c154a309-9e25-4e8b-8e25-ef6ce291bfe1",
  "receiverAccountId": "ad772c22-28e2-49e9-853e-04d59aed30ee",
  "currencyId": "USD",
  "funds": 100,
  "appliedAt": "2018-09-20T18:52:09.625Z"
}
```

For creating new `Transfer` may be provided just `senderAccountId`, `receiverAccountId`, 
`currencyId` and `funds`.

```json
{
  "senderAccountId": "c154a309-9e25-4e8b-8e25-ef6ce291bfe1",
  "receiverAccountId": "ad772c22-28e2-49e9-853e-04d59aed30ee",
  "currencyId": "USD",
  "funds": "100"
}
```

If sender or receiver accounts not existed, or currency not existed, or sender 
balance less than transfer funds, transfer will be rejected.

As `Currency`, `Transfer` cannot be deleted.



